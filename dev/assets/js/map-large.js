

var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(48.30978575, 37.17038329);
  map = new google.maps.Map(document.getElementById('map-main'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // How zoomed in you want the map to start at (always required)
    zoom: 13,

    scrollwheel: false,


    // How you would like to style the map.
    // This is where you would paste any style found on Snazzy Maps.
    styles: [{}]
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(48.30978575, 37.17038329)
  });

  var contentString =

    '<a class="b-map-bubble" href="#">' +
      '<div class="b-map-bubble__title">Споруда № 4561</div>' +
      '<div class="b-map-bubble__info">Протирадіаційне укриття на території Депо ВСП ВЧД-25</div>' +
      '<div class="b-map-bubble__descrip b-map-bubble__descrip_map">вулиця Енергетиків, 28б</div>' +
      '<div class="b-map-bubble__descrip b-map-bubble__descrip_user">1024 людини</div>' +
    '</a>' ;


  infoBubble = new InfoBubble({
    minWidth: 270,
    minHeight: 160,
    content: contentString,
    shadowStyle: 1,
    padding: 12,
    arrowSize: 10,
    borderWidth: 1,
    arrowPosition: 30,
    borderRadius: 4
  });

  infoBubble.open(map, marker);


}
google.maps.event.addDomListener(window, 'load', init);
